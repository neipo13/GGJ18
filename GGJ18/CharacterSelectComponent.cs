﻿using Nez.Sprites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GGJ18.ObjectFactory;
using Nez;
using Microsoft.Xna.Framework;
using Nez.UI.Widgets;
using Nez.UI;

namespace GGJ18
{
    
    public class CharacterSelectComponent: Component, IUpdatable
    {
        Input input;
        Sprite<Characters> sprite;
        List<Sprite> arrowSprites;
        CharacterInfo[] allCharacters;
        public CharacterSelectComponent[] otherSelectedCharacters = new CharacterSelectComponent[3];
        int playerId;
        int index;
        int max;
        float lastFrameX;
        PlayerState state;
        UICanvas canvas;
        EffectLabel label;
        EffectLabel nameLabel;
        public PlayerState State => state;
        public CharacterInfo selectedCharacter => allCharacters != null && index >= 0 && index <= max ? allCharacters[index] : null;
        public int PlayerId => playerId;
        public enum PlayerState
        {
            Out,
            Joined,
            Ready
        }
        const string TextOn = "/2JOIN/2";
        const string TextOff = "";

        const float JoinScale = 2f;
        const float ReadyScale = 4f;

        public CharacterSelectComponent(CharacterInfo[] all, int index, UICanvas canvas)
        {
            allCharacters = all;
            max = all.Length - 1;
            this.canvas = canvas;
            this.index = index;
            playerId = index;
        }

        public override void onAddedToEntity()
        {
            sprite = entity.getComponent<Sprite<Characters>>();
            sprite.play((Characters)index);
            sprite.shouldRender = false;

            arrowSprites = new List<Sprite>();
            var sprites = entity.getComponents<Sprite>();
            foreach(var arrow in sprites)
            {
                var animated = arrow as Sprite<Characters>;
                if (animated == null)
                {
                    arrow.shouldRender = false;
                    arrowSprites.Add(arrow);
                }
            }
            entity.scale = new Vector2(JoinScale);
            input = entity.getComponent<Input>();

            state = PlayerState.Out;

            var style = new LabelStyle(Color.White);
            label = new EffectLabel(TextOn, style).setAlignment(Align.center).setWrap(true);
            nameLabel = new EffectLabel(TextOff, style).setAlignment(Align.center).setWrap(true).setFontScale(2f);
            label.setWidth(512 / 4);
            nameLabel.setWidth(512 / 4);
            canvas.stage.addElement(label).setX(512 / 4 * (1 + 2 * (playerId % 2)) - 512 / 8).setY(288 / 4 * (1 + 2 * (playerId > 1 ? 1 : 0)));
            canvas.stage.addElement(nameLabel).setX(512 / 4 * (1 + 2 * (playerId % 2)) - 512 / 8).setY(288 / 4 * (1 + 2 * (playerId > 1 ? 1 : 0)) - 32);
        }

        public bool enabled => true;

        public int updateOrder => 1;

        public void update()
        {
            switch (state)
            {
                case PlayerState.Out:
                    OutUpdate();
                    break;
                case PlayerState.Joined:
                    JoinUpdate();
                    break;
                case PlayerState.Ready:
                    ReadyUpdate();
                    break;
            }
        }

        protected void OutUpdate()
        {
            if (input.Button1Input.isPressed)
            {
                sprite.shouldRender = true;
                label.setText(TextOff);
                nameLabel.setText(selectedCharacter.name);
                state = PlayerState.Joined;
                for (var i = 0; i < arrowSprites.Count; i++)
                {
                    arrowSprites[i].shouldRender = true;
                }

                while (otherSelectedCharacters.Where(c => c.State != PlayerState.Out).Select(c => c.selectedCharacter).ToArray().contains(selectedCharacter))
                {
                    index++;
                    if (index > max) index = 0;
                    nameLabel.setText(selectedCharacter.name);
                    sprite.play((Characters)index);
                }
            }
        }

        protected void JoinUpdate()
        {
            var rightPressed = input.axialInput.X > 0 && lastFrameX <= 0;
            var leftPressed = input.axialInput.X < 0 && lastFrameX >= 0;
            if (rightPressed)
            {
                do
                {
                    index++;
                    if (index > max) index = 0;
                    nameLabel.setText(selectedCharacter.name);
                    sprite.play((Characters)index);
                }
                while (otherSelectedCharacters.Where(c => c.State != PlayerState.Out).Select(c => c.selectedCharacter).ToArray().contains(selectedCharacter));
            }
            if (leftPressed)
            {
                do
                {
                    index--;
                    if (index < 0) index = max;
                    nameLabel.setText(selectedCharacter.name);
                    sprite.play((Characters)index);
                }
                while (otherSelectedCharacters.Where(c => c.State != PlayerState.Out).Select(c => c.selectedCharacter).ToArray().contains(selectedCharacter));
            }
            lastFrameX = input.axialInput.X;

            if (input.Button2Input.isPressed)
            {
                sprite.shouldRender = false;
                state = PlayerState.Out;
                label.setText(TextOn);
                nameLabel.setText(TextOff);
                for (var i = 0; i < arrowSprites.Count; i++)
                {
                    arrowSprites[i].shouldRender = false;
                }
            }
            if (input.Button1Input.isPressed)
            {
                state = PlayerState.Ready;
                entity.tweenScaleTo(ReadyScale).setEaseType(Nez.Tweens.EaseType.ElasticOut).start();
                for(var i = 0; i < arrowSprites.Count; i++)
                {
                    arrowSprites[i].shouldRender = false;
                }
            }
        }

        protected void ReadyUpdate()
        {
            if (input.Button2Input.isPressed)
            {
                for (var i = 0; i < arrowSprites.Count; i++)
                {
                    arrowSprites[i].shouldRender = true;
                }
                state = PlayerState.Joined;
                entity.tweenScaleTo(JoinScale).setEaseType(Nez.Tweens.EaseType.ElasticIn).start();
            }
        }
    }
}
