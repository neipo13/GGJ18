﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Tiled;
using Nez.Sprites;
using Nez.Textures;
using GGJ18.Effects;

namespace GGJ18
{
    public class ObjectFactory
    {
        public static PlayerState CreatePlayer(Scene scene, int playerId, TiledObject spawnObj, CharacterInfo characterInfo)
        {
            var player = scene.addEntity(new Entity($"player{playerId}")).setPosition(spawnObj.position);

            var state = player.addComponent<PlayerState>();

            var sprite = player.addComponent(new Sprite<PlayerAnims>());
            var texture = scene.content.Load<Texture2D>(characterInfo.sprite);
            var subtextures = Subtexture.subtexturesFromAtlas(texture, 16, 16);

            //move/idle
            sprite.addAnimation(PlayerAnims.RightLeft, new SpriteAnimation(new List<Subtexture>() { subtextures[4], subtextures[5], subtextures[6], subtextures[7] }));
            sprite.addAnimation(PlayerAnims.Up, new SpriteAnimation(new List<Subtexture>() { subtextures[0], subtextures[1], subtextures[2], subtextures[3] }));
            sprite.addAnimation(PlayerAnims.Down, new SpriteAnimation(new List<Subtexture>() { subtextures[8], subtextures[9], subtextures[10], subtextures[11] }));

            //attack
            sprite.addAnimation(PlayerAnims.RightLeftAttack, new SpriteAnimation(new List<Subtexture>() { subtextures[13] }));
            sprite.addAnimation(PlayerAnims.UpAttack, new SpriteAnimation(new List<Subtexture>() { subtextures[12] }));
            sprite.addAnimation(PlayerAnims.DownAttack, new SpriteAnimation(new List<Subtexture>() { subtextures[14] }));

            //death
            var deadAnim = new SpriteAnimation(new List<Subtexture>() { subtextures[16], subtextures[17] });
            deadAnim.loop = false;
            sprite.addAnimation(PlayerAnims.Dead, deadAnim);

            sprite.shouldRender = false;
            sprite.play(PlayerAnims.Dead);

            //var effect = scene.content.Load<Effect>("colorReplace");
            //sprite.material = new ColorReplaceMaterial(effect, new Color(190, 38, 51), new Color(210, 58, 71));

            var slash = player.addComponent(new Sprite<SwordAnims>());
            var swordTexture = scene.content.Load<Texture2D>("slash");
            var swordSubtextures = Subtexture.subtexturesFromAtlas(swordTexture, 32, 16);
            foreach(Subtexture sub in swordSubtextures)
            {
                sub.origin = new Vector2(24, 8);
            }
            slash.localOffset = new Vector2(0, 16);
            slash.addAnimation(SwordAnims.Off, new SpriteAnimation(new List<Subtexture>() { subtextures[15] }));
            slash.addAnimation(SwordAnims.Slash, new SpriteAnimation(swordSubtextures).setFps(20).setLoop(false));
            slash.play(SwordAnims.Off);
            slash.onAnimationCompletedEvent += (anim) =>
            {
                if (anim == SwordAnims.Slash)
                    slash.play(SwordAnims.Off);
            };
            

            player.addComponent<AfterImage>();

            var hurtBox = player.addComponent<BoxCollider>(new BoxCollider(16,16));
            hurtBox.name = Strings.HurtBox;
            hurtBox.collidesWithLayers = PhysicsLayers.PLAYER_HIT;
            hurtBox.physicsLayer = PhysicsLayers.PLAYER_HURT;
            hurtBox.active = true;
            hurtBox.isTrigger = true;

            var moveBox = player.addComponent<BoxCollider>(new BoxCollider(8, 8));
            moveBox.name = Strings.MoveBox;
            moveBox.collidesWithLayers = PhysicsLayers.MAP;
            moveBox.physicsLayer = PhysicsLayers.PLAYER_MOVE;
            
            var hitBox = player.addComponent<BoxCollider>(new BoxCollider(32, 16));
            hitBox.name = Strings.HitBox + "UD";
            hitBox.physicsLayer = PhysicsLayers.PLAYER_HIT;
            hitBox.collidesWithLayers = PhysicsLayers.PLAYER_HURT;
            hitBox.active = false;
            hitBox.isTrigger = true;


            var hitBoxLR = player.addComponent<BoxCollider>(new BoxCollider(16, 32));
            hitBoxLR.name = Strings.HitBox + "LR";
            hitBoxLR.physicsLayer = PhysicsLayers.PLAYER_HIT;
            hitBoxLR.collidesWithLayers = PhysicsLayers.PLAYER_HURT;
            hitBoxLR.active = false;
            hitBoxLR.isTrigger = true;

            player.addComponent(new PlayerHitComponent());

            player.addComponent<Mover>();
            player.addComponent(new Input(playerId));

            var controller = player.addComponent<PlayerController>();
            player.addComponent<AttackController>();

            return state;
        }
        
        public enum Characters
        {
            One,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten
        }
        public static Entity CreateCharacterSelect(Scene scene, int playerId, CharacterInfo[] characterOptions, UICanvas canvas)
        {
            var e = scene.addEntity(new Entity());
            e.scale = new Vector2(2);
            var sprite = e.addComponent(new Sprite<Characters>());
            for(var i = 0; i < characterOptions.Length; i++)
            {
                var texture = scene.content.Load<Texture2D>(characterOptions[i].sprite);
                var subtextures = Subtexture.subtexturesFromAtlas(texture, 16, 16);
                sprite.addAnimation((Characters)i, new SpriteAnimation(new List<Subtexture>() { subtextures[8], subtextures[9], subtextures[10], subtextures[11] }));
            }
            sprite.play((Characters)playerId);

            var input = e.addComponent(new Input(playerId));
            input.SetupInput();
            var tex = scene.content.Load<Texture2D>("arrow");
            var rightArrow = e.addComponent(new Sprite(tex));
            var leftArrow = e.addComponent(new Sprite(tex));
            rightArrow.localOffset = new Vector2(16 * e.scale.X, 0);
            leftArrow.localOffset = new Vector2(-16 * e.scale.X, 0);
            leftArrow.flipX = true;

            e.addComponent(new CharacterSelectComponent(characterOptions, playerId, canvas));
            e.position = new Vector2(512 / 4 * (1 + 2 * (playerId % 2)), 288 / 4 * (1 + 2 * (playerId > 1 ? 1 : 0)));
            return e;
        }
        
    }
}
