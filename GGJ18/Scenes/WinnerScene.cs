﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.UI;
using Nez.UI.Widgets;

namespace GGJ18.Scenes
{
    public class WinnerScene : Scene
    {
        protected int winningPlayerId;
        protected Input[] input = new Input[4];
        bool done = false;
        public WinnerScene(int winningPlayerId)
        {
            this.winningPlayerId = winningPlayerId;
        }

        public override void initialize()
        {
            base.initialize();
            clearColor = Color.Black;
            var renderer = addRenderer(new DefaultRenderer());
        }

        public override void onStart()
        {
            base.onStart();
            var entity = addEntity(new Entity());
            input[0] = entity.addComponent(new Input(0));
            input[1] = entity.addComponent(new Input(1));
            input[2] = entity.addComponent(new Input(2));
            input[3] = entity.addComponent(new Input(3));

            var canvas = entity.addComponent(new UICanvas());
            canvas.setRenderLayer(-10);
            var textWidth = 256 * 2;
            var textHeight = 144 * 2;
            var table = canvas
                .stage
                .addElement(new Table())
                .setFillParent(true);
            table.setBounds(0, 0, textWidth, textHeight);

            var winnerLabel = new EffectLabel($"/4/1PLAYER {winningPlayerId + 1} WINS!!!/4/1");
            winnerLabel.setWidth(textWidth);
            winnerLabel.setHeight(textHeight);
            winnerLabel.setAlignment(Align.center).setFontScale(4.0f);
            table.add(winnerLabel).setAlign(Align.center).setFillX().setFillY();


            var style = new LabelStyle(Color.White);
        }

        public override void update()
        {
            base.update();
            if (done) return;
            var p1 = input[0].Button1Input.isPressed || input[0].Button5Input.isPressed;
            var p2 = input[1].Button1Input.isPressed || input[1].Button5Input.isPressed;
            var p3 = input[2].Button1Input.isPressed || input[2].Button5Input.isPressed;
            var p4 = input[3].Button1Input.isPressed || input[3].Button5Input.isPressed;
            if (p1 || p2 || p3 || p4)
            {
                done = true;
                Core.startSceneTransition(new FadeTransition(() => new MenuScene()));
            }
        }
    }
}
