﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Nez.Sprites;
using Microsoft.Xna.Framework;
using Nez.UI;
using Nez.UI.Widgets;
using System.IO;
using System.Web.Script.Serialization;

namespace GGJ18.Scenes
{
    public class PlayerSelectScene : Scene
    {
        
        protected CharacterInfo[] allCharacterInfo;
        protected Entity[] characterSelectEntities = new Entity[4];
        protected CharacterSelectComponent[] characterSelectComponents = new CharacterSelectComponent[4];
        bool transition = false;

        float transitionTimer;
        float transitinoTime = 4.0f;

        Label countdownLabel;
        public override void initialize()
        {
            clearColor = Color.Black;
            var renderer = addRenderer(new DefaultRenderer());

            using (StreamReader reader = new StreamReader("characters.json"))
            {
                string json = reader.ReadToEnd();
                allCharacterInfo = new JavaScriptSerializer().Deserialize<CharacterInfo[]>(json);
            }
            var entity = addEntity(new Entity());

            var canvas = entity.addComponent(new UICanvas());
            canvas.setRenderLayer(-10);

            var style = new LabelStyle(Color.White);
            countdownLabel = new Label("", style).setAlignment(Align.center).setFontScale(5f);
            countdownLabel.setWidth(512 / 4);
            canvas.stage.addElement(countdownLabel).setX(512 / 2 - 512 / 8).setY(288 / 2);
            for (var i = 0; i < 4; i++)
            {
                characterSelectEntities[i] = ObjectFactory.CreateCharacterSelect(this, i, allCharacterInfo, canvas);
                characterSelectComponents[i] = characterSelectEntities[i].getComponent<CharacterSelectComponent>();
            }
            for(var i = 0; i < 4; i++)
            {
                characterSelectComponents[i].otherSelectedCharacters = characterSelectComponents.Where(c => c.PlayerId != i).ToArray();
            }
            
        }

        public override void update()
        {
            base.update();
            UpdateTransition();
            var startTransition = false;

            var selects = characterSelectEntities.Select(e => e.getComponent<CharacterSelectComponent>());
            if(selects.All(csc => csc.State == CharacterSelectComponent.PlayerState.Out || csc.State == CharacterSelectComponent.PlayerState.Ready))
            {
                var readys = characterSelectEntities
                    .Where(e => e.getComponent<CharacterSelectComponent>().State == CharacterSelectComponent.PlayerState.Ready)
                    .ToArray();
                if (readys.Length <= 1) return;
                if(!transition)
                {
                    startTransition = true;
                    transition = true;
                }
            }

            if (startTransition)
                transitionTimer = transitinoTime;
        }

        void UpdateTransition()
        {
            if (!transition) return;
            transitionTimer -= Time.deltaTime;
            var selects = characterSelectEntities.Select(e => e.getComponent<CharacterSelectComponent>());
            if (transitionTimer <= 0f)
            {
                if (selects.All(csc => csc.State == CharacterSelectComponent.PlayerState.Out || csc.State == CharacterSelectComponent.PlayerState.Ready))
                {
                    start();
                }
                else
                {
                    transition = false;
                    countdownLabel.setText("");
                }
            }
            else
            {
                if (selects.All(csc => csc.State == CharacterSelectComponent.PlayerState.Out || csc.State == CharacterSelectComponent.PlayerState.Ready))
                {
                    countdownLabel.setText(Mathf.ceil(transitionTimer).ToString());
                }
                else if(countdownLabel.getText() != "")
                {
                    transition = false;
                    countdownLabel.setText("");
                }
            }
        }

        public void start()
        {
            var players = new List<int>();
            CharacterInfo[] selectedCharacterInfo = new CharacterInfo[4];
            var readyPlayersInfo = characterSelectEntities
                    .Select(e => e.getComponent<CharacterSelectComponent>())
                    .Where(e => e.State == CharacterSelectComponent.PlayerState.Ready)
                    .ToArray();
            for (var i = 0; i < 4; i++)
            {
                if(readyPlayersInfo.Select(inf => inf.PlayerId).Contains(i))
                {
                    players.Add(i);
                    var info = readyPlayersInfo.Single(rpi => rpi.PlayerId == i);
                    selectedCharacterInfo[i] = info.selectedCharacter;
                }
            }
            var dictionary = new Dictionary<int, int>();
            for (var i = 0; i < players.Count; i++)
            {
                dictionary.Add(players[i], 0);
            }
            Core.scene = new GameScene(players.ToArray<int>(), dictionary, 3, selectedCharacterInfo);
        }
    }
}
