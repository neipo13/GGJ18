﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.UI;

namespace GGJ18.Scenes
{
    public class ResultsScene : Scene
    {
        protected int[] players;
        protected Dictionary<int, int> playerWins;
        protected int maxWins;
        protected Input[] input = new Input[4];
        bool done = false;
        protected CharacterInfo[] selectedCharacterInfo = new CharacterInfo[4];
        public ResultsScene(int[] players, Dictionary<int, int> playerWins, int maxWins, CharacterInfo[] characters)
        {
            this.players = players;
            this.playerWins = playerWins;
            this.maxWins = maxWins;
            this.selectedCharacterInfo = characters;
        }

        public override void initialize()
        {
            base.initialize();
            clearColor = Color.Black;
            var renderer = addRenderer(new DefaultRenderer());
        }

        public override void onStart()
        {
            base.onStart();
            var entity = addEntity(new Entity());
            input[0] = entity.addComponent(new Input(0));
            input[1] = entity.addComponent(new Input(1));
            input[2] = entity.addComponent(new Input(2));
            input[3] = entity.addComponent(new Input(3));

            var canvas = entity.addComponent(new UICanvas());
            canvas.setRenderLayer(-10);
            var textWidth = 256 * 2;
            var textHeight = 144 * 2;
            var table = canvas
                .stage
                .addElement(new Table())
                .setFillParent(true);
            table.setBounds(0, 0, textWidth, textHeight);
            table.add(new Label("Score").setFontScale(2.5f));
            foreach (int player in players)
            {
                table.row();
                table.add(new Label($"Player {player + 1}: {playerWins[player]}/{maxWins}").setFontScale(2f));

            }


            var style = new LabelStyle(Color.White);
        }

        public override void update()
        {
            base.update();
            if (done) return;
            var transitioning = false;
            for (var i = 0; i < input.Length; i++)
            {
                if (input[i].Button1Input.isPressed || input[i].Button5Input.isPressed)
                {
                    transitioning = true;
                }
            }
            if(transitioning && !done)
            {
                done = true;
                Core.scene=new GameScene(players, playerWins, maxWins, selectedCharacterInfo);
            }
        }
    }
}
