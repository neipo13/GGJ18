﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez.Tiled;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace GGJ18.Scenes
{
    public class GameScene : Scene
    {
        protected int[] players;
        protected List<Tuple<int, PlayerState>> states;
        protected Dictionary<int, int> playerWins;
        protected int maxWins;
        protected TiledMap tiledMap;
        protected TiledTileLayer collisionLayer;
        protected bool gameOver = false;
        protected CharacterInfo[] selectedCharacterInfo = new CharacterInfo[4];

        public GameScene(int[] players, Dictionary<int, int> playerWins, int maxWins, CharacterInfo[] characters)
        {
            this.players = players;
            this.playerWins = playerWins;
            this.maxWins = maxWins;
            this.states = new List<Tuple<int, PlayerState>>();
            this.selectedCharacterInfo = characters;
        }

        public override void initialize()
        {
            base.initialize();

            addRenderer(new DefaultRenderer());
            clearColor = new Color(248, 163, 61);

            var mapSelection = Nez.Random.range(1, 8);
            tiledMap = content.Load<TiledMap>($"map{mapSelection}");
            var tiledEntity = this.createEntity("debugMap");
            var tiledMapComponent = tiledEntity.addComponent(new TiledMapComponent(tiledMap, Strings.Collision));
            tiledMapComponent.setLayersToRender(Strings.Collision);
            collisionLayer = tiledMapComponent.collisionLayer;
            tiledMapComponent.physicsLayer = PhysicsLayers.MAP;

            var tileBackground = tiledEntity.addComponent(new TiledMapComponent(tiledMap));
            tileBackground.setLayersToRender("background");
            tileBackground.renderLayer = 10;


            var tiledDecorativeComponent = tiledEntity.addComponent(new TiledMapComponent(tiledMap));
            tiledDecorativeComponent.setLayersToRender("decorative");
            tiledDecorativeComponent.renderLayer = -1;
            var offscreenColliders = addEntity(new Entity());
            offscreenColliders.addComponent(new BoxCollider(-32, 112, 32, 32));
            offscreenColliders.addComponent(new BoxCollider(256, 112, 32, 32));
            
        }

        public override void onStart()
        {
            base.onStart();
            var spawns = tiledMap.getObjectGroup("spawn");
            for (var i = 0; i < spawns.objects.Length; i++)
            {
                int playerId = int.Parse(spawns.objects[i].name);
                if (players.contains(playerId))
                {
                    var p = ObjectFactory.CreatePlayer(this, playerId, spawns.objects[i], selectedCharacterInfo[i]);
                    states.Add(new Tuple<int, PlayerState>(playerId, p));
                }
            }
        }
        public override void update()
        {
            base.update();
            

            if (!gameOver && states.Where(s => s.Item2.alive).Count() == 1)
            {
                gameOver = true;
                Console.WriteLine("WINNER WINNER CHICKEN DINNER");
                var winningId = states.Single(s => s.Item2.alive).Item1;
                int wins = playerWins[winningId];
                wins++;
                playerWins[winningId] = wins;
                Core.schedule(3f, (a) =>
                {
                    if (wins >= maxWins)
                        Core.startSceneTransition(new FadeTransition(() => new WinnerScene(winningId)));
                    else
                        Core.startSceneTransition(new FadeTransition(() => new ResultsScene(players, playerWins, maxWins, selectedCharacterInfo)));
                });
            }
        }
    }
}
