﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Nez.Sprites;
using Nez.UI;
using Microsoft.Xna.Framework;
using Nez.UI.Widgets;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Web.Script.Serialization;

namespace GGJ18.Scenes
{

    public class MenuScene : Scene
    {
        enum MenuState
        {
            Title,
            Menu,
            Options
        }
        MenuState state = MenuState.Title;
        Entity entity;
        UICanvas canvas;
        Input input1;
        
        EffectLabel pressStart;

        TextButton vs;
        TextButton options;
        TextButton quit;

        EffectLabel fullScreen;
        EffectLabel music;
        EffectLabel sfx;

        Slider musicVolume;
        Slider sfxVolume;
        TextButton fullScreenToggle;
        TextButton backToMenu;

        Sprite logo;
        Vector2 logoPosTitle = new Vector2(512 / 2, 288 / 2);
        Vector2 logoPosMenu = new Vector2(512 / 2, 288 * 1 / 4);

        Settings settings = new Settings();
        string fullScreenButtonText => settings.fullscreen ? "ON" : "OFF";
        
        public override void initialize()
        {
            clearColor = Color.Black;
            var renderer = addRenderer(new DefaultRenderer());

            using (StreamReader reader = new StreamReader("settings.json"))
            {
                string json = reader.ReadToEnd();
                settings = new JavaScriptSerializer().Deserialize<Settings>(json);
            }

            entity = addEntity(new Entity());

            input1 = entity.addComponent(new Input(0));
            entity.addComponent(new Input(1));
            entity.addComponent(new Input(2));
            entity.addComponent(new Input(3));

            canvas = entity.addComponent(new UICanvas());
            canvas.setRenderLayer(-10);
            
            var style = new LabelStyle(Color.White);
            var menuStyle = TextButtonStyle.create(Color.Black, Color.Red, Color.Orange);
            var texture = content.Load<Texture2D>("logo");
            logo = entity.addComponent<Sprite>(new Sprite(texture));
            logo.localOffset = logoPosTitle;
            //var textLabel = new EffectLabel("NINJA /1/4VANISH/3/1", style).setAlignment(Align.center);
            pressStart = new EffectLabel("/2Press Start/2", style).setAlignment(Align.center);

            vs = new TextButton("", menuStyle);
            vs.shouldUseExplicitFocusableControl = true;
            options = new TextButton("", menuStyle);
            options.shouldUseExplicitFocusableControl = true;
            quit = new TextButton("", menuStyle);
            quit.shouldUseExplicitFocusableControl = true;

            vs.gamepadRightElement = options;
            vs.gamepadDownElement = options;  
            options.gamepadRightElement = quit;           
            options.gamepadDownElement = quit;
            options.gamepadLeftElement = vs;
            options.gamepadUpElement = vs;
            quit.gamepadLeftElement = options;
            quit.gamepadUpElement = options;

            vs.onClicked += Vs_onClicked;
            options.onClicked += Options_onClicked;
            quit.onClicked += Quit_onClicked;

            var menuGroup = new VerticalGroup(5f);
            menuGroup.addElement(vs);
            menuGroup.addElement(options);
            menuGroup.addElement(quit);

            fullScreen = new EffectLabel("").setAlignment(Align.right);
            music = new EffectLabel("").setAlignment(Align.right);
            sfx = new EffectLabel("").setAlignment(Align.right);

            fullScreenToggle = new TextButton(fullScreenButtonText, menuStyle);
            fullScreenToggle.shouldUseExplicitFocusableControl = true;
            backToMenu = new TextButton("BACK", menuStyle);
            backToMenu.shouldUseExplicitFocusableControl = true;
            backToMenu.setIsVisible(false);
            musicVolume = new Slider(0f, 100f, 10f, false, SliderStyle.create(Color.White, Color.Orange));
            musicVolume.setValue(settings.music);
            musicVolume.shouldUseExplicitFocusableControl = true;
            musicVolume.onChanged += MusicVolume_onChanged;
            sfxVolume = new Slider(0f, 100f, 10f, false, SliderStyle.create(Color.White, Color.Orange));
            sfxVolume.setValue(settings.sfx);
            sfxVolume.shouldUseExplicitFocusableControl = true;
            sfxVolume.onChanged += SfxVolume_onChanged;

            fullScreenToggle.gamepadRightElement = musicVolume;
            fullScreenToggle.gamepadDownElement = musicVolume;
            musicVolume.gamepadDownElement = sfxVolume;
            musicVolume.gamepadUpElement = fullScreenToggle;
            sfxVolume.gamepadUpElement = musicVolume;
            sfxVolume.gamepadDownElement = backToMenu;
            backToMenu.gamepadLeftElement = sfxVolume;
            backToMenu.gamepadUpElement = sfxVolume;

            backToMenu.onClicked += BackToMenu_onClicked;
            fullScreenToggle.onClicked += FullScreenToggle_onClicked;

            fullScreenToggle.setIsVisible(false);
            musicVolume.setIsVisible(false);
            sfxVolume.setIsVisible(false);

            var optionsTable = new Table();
            optionsTable.row();
            optionsTable.add(fullScreen);
            optionsTable.add(fullScreenToggle);
            optionsTable.row();
            optionsTable.add(music);
            optionsTable.add(musicVolume);
            optionsTable.row();
            optionsTable.add(sfx);
            optionsTable.add(sfxVolume);
            optionsTable.row();
            optionsTable.add(new Label(" "));
            optionsTable.add(backToMenu);
           
            
            canvas.stage.addElement(pressStart).setX(512/2).setY(288 * 3/4);
            canvas.stage.addElement(menuGroup).setX(512 / 2).setY(288 * 3 / 5);
            canvas.stage.addElement(optionsTable).setX(512 / 2).setY(288 * 3 / 5);


        }

        private void FullScreenToggle_onClicked(Button obj)
        {
            settings.fullscreen = !settings.fullscreen;
            Screen.isFullscreen = settings.fullscreen;
            Screen.applyChanges();
            fullScreenToggle.setText(fullScreenButtonText);
        }

        private void BackToMenu_onClicked(Button obj)
        {
            OptionsToMenu();
        }

        private void MusicVolume_onChanged(float obj)
        {
            settings.music = obj;
            float normalized = settings.music / 100f;
            MediaPlayer.Volume = (Mathf.exp(normalized) - 1f) / ((float)Math.E - 1f);
        }

        private void SfxVolume_onChanged(float obj)
        {
            settings.sfx = obj;
        }

        private void Options_onClicked(Button obj)
        {
            MenuToOptions();
        }

        private void Quit_onClicked(Button obj)
        {
            Core.exit();
        }

        private void Vs_onClicked(Button obj)
        {
            Core.startSceneTransition(new FadeTransition(() => new PlayerSelectScene()));
        }

        public override void update()
        {
            base.update();
            switch (state)
            {
                case MenuState.Title:
                    TitleUpdate();
                    break;
                case MenuState.Menu:
                    MenuUpdate();
                    break;
                case MenuState.Options:
                    OptionsUpdate();
                    break;
            }
            //var components = entity.getComponents<Input>();
            //var transitioning = false;
            //for (var i = 0; i < components.Count; i++)
            //{
            //    if(components[i].Button1Input.isPressed || components[i].Button5Input.isPressed)
            //    {
            //        transitioning = true;
            //    }
            //}
            //if (transitioning)
            //{
            //    Core.startSceneTransition(new FadeTransition(() => new PlayerSelectScene()));
            //}
        }

        private void TitleUpdate()
        {
            if (state != MenuState.Title) return;
            var components = entity.getComponents<Input>();
            for (var i = 0; i < components.Count; i++)
            {
                if (components[i].Button1Input.isPressed)
                {
                    TitleToMenu();
                }
            }
        }
        private void MenuUpdate()
        {
            if (state != MenuState.Menu) return;
            var components = entity.getComponents<Input>();
            for (var i = 0; i < components.Count; i++)
            {
                if (components[i].Button2Input.isPressed)
                {
                    MenuToTitle();
                }
            }
        }

        private void OptionsUpdate()
        {
            if (state != MenuState.Options) return;
            var components = entity.getComponents<Input>();
            for (var i = 0; i < components.Count; i++)
            {
                if (components[i].Button2Input.isPressed)
                {
                    OptionsToMenu();
                }
            }
        }

        private void TitleToMenu()
        {
            float timeToTransfer = 0.5f;
            state = MenuState.Menu;
            pressStart.setText("");
            logo.tween("localOffset", logoPosMenu, timeToTransfer).setEaseType(Nez.Tweens.EaseType.BounceOut).start();
            Core.schedule(timeToTransfer, (timer) =>
            {
                canvas.stage.keyboardEmulatesGamepad = true;
                canvas.stage.keyboardActionKey = (Keys)input1.mapping.key1;
                canvas.stage.setGamepadFocusElement(vs);
                vs.setText("VS");
                options.setText("OPTIONS");
                quit.setText("QUIT");
            });
        }
        private void MenuToTitle()
        {
            float timeToTransfer = 0.5f;
            state = MenuState.Title;
            canvas.stage.keyboardEmulatesGamepad = false;
            canvas.stage.keyboardActionKey = (Keys)input1.mapping.key1;
            canvas.stage.setGamepadFocusElement(null);
            vs.setText("");
            options.setText("");
            quit.setText("");
            logo.tween("localOffset", logoPosTitle, timeToTransfer).setEaseType(Nez.Tweens.EaseType.BounceOut).start();
            Core.schedule(timeToTransfer, (timer) =>
            {
                pressStart.setText("/2Press Start/2");
            });

        }
        private void MenuToOptions()
        {
            state = MenuState.Options;


            fullScreenToggle.setIsVisible(true);
            musicVolume.setIsVisible(true);
            sfxVolume.setIsVisible(true);

            canvas.stage.setGamepadFocusElement(fullScreenToggle);
            vs.setText("");
            options.setText("");
            quit.setText("");

            fullScreen.setText("FULLSCREEN  :");
            music.setText("MUSIC VOLUME:");
            sfx.setText("SFX VOLUME   :");
            backToMenu.setVisible(true);


        }
        private void OptionsToMenu()
        {
            state = MenuState.Menu;
            //save the settings

            using (StreamWriter writer = new StreamWriter("settings.json", false))
            {
                string json = new JavaScriptSerializer().Serialize(settings);
                writer.WriteLine(json);
            }
            fullScreenToggle.setIsVisible(false);
            musicVolume.setIsVisible(false);
            sfxVolume.setIsVisible(false);
            fullScreen.setText("");
            music.setText("");
            sfx.setText("");
            backToMenu.setVisible(false);

            canvas.stage.setGamepadFocusElement(vs);
            vs.setText("VS");
            options.setText("OPTIONS");
            quit.setText("QUIT");

        }
    }
}
