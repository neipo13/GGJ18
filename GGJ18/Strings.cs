﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGJ18
{
    public class Strings
    {
        public static string HitBox = "hitbox";
        public static string HurtBox = "hurtbox";
        public static string MoveBox = "movebox";
        public static string Spawn = "spawn";
        public static string Collision = "collision";
    }
}
