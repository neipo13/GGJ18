﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Nez.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace GGJ18
{
    public class AttackController : Component, IUpdatable
    {
        Input input;
        BoxCollider[] hitBoxes;
        PlayerState state;
        Sprite<PlayerAnims> sprite;
        Sprite<SwordAnims> swordSprite;
        AfterImage afterImage;
        SoundEffect slash;
        float attackPause = 0.75f;
        float attackTimer;
        bool displaying = false;
        Settings settings = new Settings();
        float volume => (Mathf.exp(settings.sfx/100f) - 1f) / ((float)System.Math.E - 1f);

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            input = entity.getComponent<Input>();
            state = entity.getComponent<PlayerState>();
            hitBoxes = entity.getComponents<BoxCollider>().Where(b => b.name.Contains(Strings.HitBox)).ToArray();
            sprite = entity.getComponent<Sprite<PlayerAnims>>();
            swordSprite = entity.getComponent<Sprite<SwordAnims>>();
            afterImage = entity.getComponent<AfterImage>();
            afterImage.Activate(entity.position, new Vector2(), true, attackPause);
            slash = entity.scene.content.Load<SoundEffect>("slash_sound");
            Core.schedule(attackPause, (a) => afterImage.Deactivate());

            using (var reader = new System.IO.StreamReader("settings.json"))
            {
                string json = reader.ReadToEnd();
                settings = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Settings>(json);
            }
        }

        public override void onRemovedFromEntity()
        {
            base.onRemovedFromEntity();
            afterImage.Deactivate();
        }

        public void update()
        {
            if (!state.alive)
            {
                if (afterImage.active) afterImage.Deactivate();
                return;
            }
            if(attackTimer > 0f)
            {
                attackTimer -= Time.deltaTime;
                if(attackTimer <= 0f)
                {
                    state.canMove = true;
                    afterImage.Deactivate();
                    displaying = false;
                }
            }
            if (input.Button1Input.isPressed)
            {
                SpawnDisplay(1f, state.velocity/5f);
            }
            if (input.Button2Input.isPressed)
            {
                attackTimer = attackPause;
                swordSprite.play(SwordAnims.Slash);
                var hitbox = state.SetSwordInfo(ref swordSprite, ref hitBoxes);
                state.canMove = false;
                sprite.play(state.AnimationToUse());
                SpawnDisplay(attackTimer, attacking:true);
                slash.Play(volume, 0f, 0f);
                Core.schedule(0.25f, (a) => hitbox.active = false);
            }
            sprite.play(state.AnimationToUse());
            sprite.flipX = state.flipX;
        }

        public void SpawnDisplay(float timer = 0f, Vector2 movememt = new Vector2(), bool attacking = false)
        {
            //if (displaying && !attacking) return;
            //displaying = true;
            afterImage.Activate(entity.position, movememt, !attacking, timer);
            if(attacking) attackTimer = timer;
            
        }
    }
}
