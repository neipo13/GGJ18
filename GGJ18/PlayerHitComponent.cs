﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Nez.Sprites;

namespace GGJ18
{
    public class PlayerHitComponent : Component, ITriggerListener
    {
        Sprite<PlayerAnims> sprite;
        PlayerState state;
        SoundEffect hitEffect;
        Settings settings = new Settings();
        float volume => (Mathf.exp(settings.sfx / 100f) - 1f) / ((float)System.Math.E - 1f);

        public override void onAddedToEntity()
        {
            state = entity.getComponent<PlayerState>();
            sprite = entity.getComponent<Sprite<PlayerAnims>>();
            hitEffect = entity.scene.content.Load<SoundEffect>("hit_sound");

            using (var reader = new System.IO.StreamReader("settings.json"))
            {
                string json = reader.ReadToEnd();
                settings = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Settings>(json);
            }
        }
        public void onTriggerEnter(Collider other, Collider local)
        {
            if (other.entity.name == local.entity.name) return;
            if (local.name.Contains(Strings.HitBox)) return;
            Console.WriteLine($"OUCH {local.entity.name}");

            hitEffect.Play(volume, 0f, 0f);
            sprite.play(PlayerAnims.Dead);
            sprite.shouldRender = true;
            state.alive = false;

        }

        public void onTriggerExit(Collider other, Collider local)
        {
        }

        public void onTriggerStay(Collider other, Collider local)
        {
        }
    }
}
