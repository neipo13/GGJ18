﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGJ18
{
    public class Settings
    {
        public Settings()
        {
            fullscreen = true;
            music = 100f;
            sfx = 100f;
        }
        public bool fullscreen { get; set; }
        public float music { get; set; }
        public float sfx { get; set; }
    }
}
