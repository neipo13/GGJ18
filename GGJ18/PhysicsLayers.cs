﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGJ18
{
    public class PhysicsLayers
    {
        public static int MAP = 1 << 1;
        public static int PLAYER_MOVE = 1 << 2;
        public static int PLAYER_HURT = 1 << 3;
        public static int PLAYER_HIT = 1 << 4;
    }
}
