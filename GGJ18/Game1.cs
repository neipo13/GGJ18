﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Nez;
using Nez.Console;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace GGJ18
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Nez.Core
    {
        Scene.SceneResolutionPolicy policy;
        Settings settings = new Settings();
        public Game1() : base(windowTitle: "Ninja Vanish")
        {
            using (StreamReader reader = new StreamReader("settings.json"))
            {
                string json = reader.ReadToEnd();
                settings = new JavaScriptSerializer().Deserialize<Settings>(json);
            }
            if (settings.fullscreen)
            {
                Screen.isFullscreen = true;
            }
            else
            {
                Screen.isFullscreen = false;
                Window.Position = new Point(0, 0);

            }
            policy = Scene.SceneResolutionPolicy.BestFit;
            Scene.setDefaultDesignResolution(512, 288, policy, 0, 0);
            
            Window.AllowUserResizing = true;
            DebugConsole.renderScale = 5;

        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            var song = content.Load<Song>("ninja");

            float normalized = settings.music / 100f;
            MediaPlayer.Volume = (Mathf.exp(normalized) - 1f) / ((float)System.Math.E - 1f);
            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = true;
            scene = Scene.createWithDefaultRenderer();
            base.Update(new GameTime());
            base.Draw(new GameTime());
            Nez.Input.maxSupportedGamePads = 4;
            scene = new Scenes.MenuScene();
            //Screen.setSize(1280, 720);
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (Nez.Input.isKeyPressed(Keys.F11))
            {
                Screen.isFullscreen = !Screen.isFullscreen;
                Screen.applyChanges();
                if (Screen.isFullscreen)
                {
                    Screen.setSize(1280, 720);
                }
            }
        }
    }
}
