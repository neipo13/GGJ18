﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GGJ18.Effects
{
    public class ColorReplaceMaterial : Material
    {
        public ColorReplaceMaterial(Effect effect, Color replace1, Color replace2) : base(effect)
        {
            effect.Parameters["_color"].SetValue(replace1.ToVector4());
            effect.Parameters["_color2"].SetValue(replace2.ToVector4());
            effect.Parameters["_replaceColor"].SetValue(new Vector4(157, 157, 157, 0));
            effect.Parameters["_replaceColor2"].SetValue(new Vector4(178, 220, 239, 0));
        }



    }
}
