﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Microsoft.Xna.Framework.Graphics;

namespace GGJ18
{
    public class AfterImage : RenderableComponent, IUpdatable
    {
        Sprite<PlayerAnims> sprite;
        Subtexture texture;
        Vector2 imagePos;
        Vector2 velocity = new Vector2();
        SpriteEffects spriteEffects;

        float speed = 0.5f;
        float timer = 0f;
        float maxTimer = 0f;
        public bool active = false;
        bool shouldFade = false;
        public override float width { get { return 16; } }
        public override float height { get { return 16; } }

        public bool Activate(Vector2 pos, Vector2 movement = new Vector2(), bool shouldFade = false, float timer = 0f)
        {
            if (active && !this.shouldFade && shouldFade) return false;
            active = true;
            imagePos = pos;
            texture = sprite.subtexture;
            velocity = movement;
            spriteEffects = sprite.spriteEffects;
            this.maxTimer = timer;
            this.timer = timer;
            this.shouldFade = shouldFade;
            return active;
        }

        public bool Deactivate()
        {
            
            active = false;
            return active;
        }

        public override void onAddedToEntity()
        {
            sprite = entity.getComponent<Sprite<PlayerAnims>>();
            active = false;
            imagePos = Vector2.Zero;
            texture = sprite.subtexture;
        }

        public override void render(Graphics graphics, Camera camera)
        {
            if (active)
            {
                Color c;
                if (shouldFade)
                {
                    c = new Color(sprite.color, timer / maxTimer);
                }
                else
                {
                    c = sprite.color;
                }
                if(c.A > 0)
                {
                    graphics.batcher.draw(texture, imagePos, c, entity.transform.rotation, sprite.origin, entity.transform.scale, spriteEffects, sprite.layerDepth);
                }
            }
        }

        public void update()
        {
            if(timer > 0)
            {
                timer -= Time.deltaTime;
                if(timer < 0)
                {
                    timer = 0;
                }
            }
            if(active && (Math.Abs(velocity.X) > 0 || Math.Abs(velocity.Y) > 0))    
                imagePos += velocity * Time.deltaTime * speed;
        }
    }
}
