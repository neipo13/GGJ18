﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGJ18
{
    public class CharacterInfo
    {

        public int id { get; set; }
        /// <summary>
        /// Display Name for Users
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Name of content to load from content manager
        /// </summary>
        public string sprite { get; set; }
        /// <summary>
        /// Would need to load from sprite crunch somehow
        /// </summary>
        public int[] frames { get; set; }
    }
}
