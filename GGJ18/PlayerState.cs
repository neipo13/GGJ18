﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework;

namespace GGJ18
{
    public enum PlayerAnims
    {
        Up,
        Down,
        RightLeft,
        UpAttack,
        DownAttack,
        RightLeftAttack,
        Dead
    }

    public enum SwordAnims
    {
        Slash,
        Off
    }
    public class PlayerState : Component
    {
        public Vector2 velocity;
        public bool canMove = true;
        public bool alive = true;

        private Direction _direction = Direction.Down;
        private bool _flipX = false;
        public bool flipX
        {
            get
            {
                if (direction == Direction.Left)
                    _flipX = true;
                else if (direction == Direction.Right)
                    _flipX = false;

                return _flipX;
            }
        }
        public Direction direction
        {
            get
            {
                var xLarger = Math.Abs(velocity.X) > Math.Abs(velocity.Y);
                if(xLarger && velocity.X > 0)
                {
                    _direction = Direction.Right;
                }
                else if( xLarger && velocity.X < 0)
                {
                    _direction = Direction.Left;
                }
                else if (!xLarger && velocity.Y > 0)
                {
                    _direction = Direction.Down;
                }
                else if (!xLarger && velocity.Y < 0)
                {
                    _direction = Direction.Up;
                }
                return _direction;
            }
        }

        public PlayerAnims AnimationToUse()
        {
            if (!canMove)
            {
                if (direction == Direction.Down)
                {
                    return PlayerAnims.DownAttack;
                }
                else if (direction == Direction.Up)
                {
                    return PlayerAnims.UpAttack;
                }
                else if (direction == Direction.Right)
                {
                    return PlayerAnims.RightLeftAttack;
                }
                else if (direction == Direction.Left)
                {
                    return PlayerAnims.RightLeftAttack;
                }
            }
            else
            {

                if (direction == Direction.Down)
                {
                    return PlayerAnims.Down;
                }
                else if (direction == Direction.Up)
                {
                    return PlayerAnims.Up;
                }
                else if (direction == Direction.Right)
                {
                    return PlayerAnims.RightLeft;
                }
                else if (direction == Direction.Left)
                {
                    return PlayerAnims.RightLeft;
                }
            }
            return PlayerAnims.Down;
        }

        public Vector2 swordOffset
        {
            get
            {
                switch (direction)
                {
                    case Direction.Down:
                        return new Vector2(0, 16);
                    case Direction.Up:
                        return new Vector2(0, -16);
                    case Direction.Left:
                        return new Vector2(-16, 0);
                    case Direction.Right:
                        return new Vector2(16, 8);
                    default:
                        return Vector2.Zero;
                }
            }
        }

        public Vector2 swordColliderOffset
        {
            get
            {
                switch (direction)
                {
                    case Direction.Down:
                        return new Vector2(0, 16);
                    case Direction.Up:
                        return new Vector2(0, -16);
                    case Direction.Left:
                        return new Vector2(-16, 0);
                    case Direction.Right:
                        return new Vector2(16, 0);
                    default:
                        return Vector2.Zero;
                }
            }
        }


        public BoxCollider SetSwordInfo(ref Nez.Sprites.Sprite<SwordAnims> sword, ref BoxCollider[] hitboxes)
        {
            sword.setLocalOffset(swordOffset);
            var hitboxLR = hitboxes.SingleOrDefault(h => h.name == Strings.HitBox + "LR");
            var hitboxUD = hitboxes.SingleOrDefault(h => h.name == Strings.HitBox + "UD");
            sword.flipX = direction == Direction.Up;
            sword.flipY = direction == Direction.Right || direction == Direction.Up;
            if(direction == Direction.Left || direction == Direction.Right)
            {
                hitboxLR.setLocalOffset(swordColliderOffset);
                sword.rotation = 90f;
                hitboxLR.active = true;
                return hitboxLR;
            }
            else
            {
                hitboxUD.setLocalOffset(swordColliderOffset);
                sword.rotation = 0f;
                hitboxUD.active = true;
                return hitboxUD;
            }

        }

    }
}
