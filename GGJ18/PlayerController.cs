﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;

namespace GGJ18
{
    public class PlayerController : Component, IUpdatable
    {
        Input input;
        Mover mover;
        CollisionResult collisionResult;
        Sprite<PlayerAnims> sprite;

        float movespeed = 120f;
        PlayerState state;

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            input = entity.getComponent<Input>();
            mover = entity.getComponent<Mover>();
            state = entity.getComponent<PlayerState>();
            sprite = entity.getComponent<Sprite<PlayerAnims>>();
        }

        public void update()
        {
            if (!state.canMove || !state.alive) return;
            state.velocity = input.axialInput;
            if (state.velocity != Vector2.Zero)
            {
                state.velocity.Normalize();
                state.velocity *= movespeed;
            }
            mover.move(state.velocity * Time.deltaTime, out collisionResult);
        }
    }
}
