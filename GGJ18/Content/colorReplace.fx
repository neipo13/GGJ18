﻿
sampler _mainTex;
//0-1.25
float4 _replaceColor;
float4 _color;
float4 _replaceColor2;
float4 _color2;

float4 frag(float2 coords : TEXCOORD0) : COLOR0
{
    float4 sampled = tex2D(_mainTex, coords);
    if (sampled.r == _replaceColor.r && sampled.g == _replaceColor.g && sampled.b == _replaceColor.b)
    {
        return _color;
    }
    if (sampled.r == _replaceColor2.r && sampled.g == _replaceColor2.g && sampled.b == _replaceColor2.b)
    {
        return _color2;
    }
    return tex2D(_mainTex, coords);
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 frag();
    }
}